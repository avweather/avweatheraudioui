outlets = 2;

function loadPoints() {
	var result = new Dict("result");
	
	const noOfPoints = result.getsize("conditions");
	
	// assertions
	if (noOfPoints < 0 || noOfPoints > 10) {
		post("Error parsing conditions: Number of points out of acceptable range.", noOfPoints);
		return;
	}
	
	for (var i = 0; i < noOfPoints; i++) {
		
		var conditionID = result.get("conditions[" + i + "]::id");
		var x = result.get("conditions[" + i + "]::localisation::lon");
		var y = result.get("conditions[" + i + "]::localisation::lat");
		
		// assertions
		if (conditionID < 0 || conditionID > 12) {
			post("Error parsing conditions: ConditionID out of acceptable range.", conditionID);
			break;
		}
		
		// add points
  		post("Adding point id", i, "with conditionID", conditionID, "to x", x, "and y", y, "\n");
		outlet(0, ["xyz", i+1, x, y, 0.0]);
		outlet(1, [conditionID, i, 1]);
	}
}
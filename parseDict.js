outlets = 3;

var GeoframeID = -1;
var response = new Dict("response");

function dictionary()
{
	var result = new Dict("result");
	result.parse(response.get("body"));
	
	const newGeoframeID = result.get("geoframeID");
	
	if (GeoframeID != newGeoframeID) {
		post("Fetched new GeoframeID", newGeoframeID);
		
		GeoframeID = newGeoframeID;
		outlet(2, "bang");
		outlet(1, "loadPoints");
		outlet(0, GeoframeID);
	}
}

function reset()
{
	GeoframeID = -1;
	var result = new Dict("result");
	result.clear();
	outlet(0, GeoframeID);
	outlet(2, "bang");
}